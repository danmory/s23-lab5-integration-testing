# Lab5 -- Integration testing

## Specs

Budet car price per minute = 16

Luxury car price per minute = 38

Fixed price per km = 15

Allowed deviations in % = 10

Inno discount in % = 9

Specs are retrieved from <https://script.google.com/macros/s/AKfycby1s4fIXUfPk8Oxue9RhFZ2_Tq2m-P2Bz5CapR3eo8KHd_c4qXwVyWfgKxwM-ZsnY8U/exec?action=get&&service=getSpec&email=d.moriakov@innopolis.university>

## Results

All work is done in [Google Tables](https://docs.google.com/spreadsheets/d/1l97iplZl_D5b4syP9GU3qmR9ENwN8goK102tzYqtqHw/edit?usp=sharing)

The bugs are the following:

1. Fixed Price logic does not work
2. Miscalculations of luxury cars prices
